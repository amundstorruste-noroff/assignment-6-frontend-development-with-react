import { Navigate } from "react-router-dom";
import { useUser } from "../context/UserContext";

/**
 * Simple higher order component that can be used to protect our routes
 * @param {*} Component 
 * @returns 
 */
const withAuth = (Component) => (props) => {
  const { user } = useUser();
  if (user !== null) {
    return <Component {...props} />;
  } else {
    return <Navigate to="/"></Navigate>;
  }
};

export default withAuth;
