const validateKey = (key) => {
    if (!key || typeof key !== "string") {
        throw new Error("Invalid storage key provided");
    }
};

/**
 * Stores a key value pair in the local storage
 * @param {*} key
 * @param {JSON} value
 */
export const saveToStorage = (key, value) => {
    validateKey(key);

    if (!value) {
        throw new Error("saveToStorage: No value provided for " + key);
    }

    sessionStorage.setItem(key, JSON.stringify(value));
};

/**
 * Reads data value stored in local storage at a given key
 * @param {*} key
 * @returns
 */
export const readFromStorage = (key) => {
    validateKey(key);

    const data = sessionStorage.getItem(key);
    // If data return stored value as JSON
    if (data) return JSON.parse(data);
    // Otherwise
    return null;
};

/**
 * Removes a key value pair from local storage
 * @param {*} key
 */
export const deleteFromStorage = (key) => {
    validateKey(key);

    sessionStorage.removeItem(key);
};
