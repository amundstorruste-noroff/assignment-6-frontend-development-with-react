import { createHeaders } from ".";
const apiUrl = process.env.REACT_APP_API_URL;

// Make new translation
export const addTranslationAsync = async (user, newTranslation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, newTranslation],
            }),
        });
        if (!response.ok) {
            throw new Error("Could not add new translation to translations.");
        }
        const result = await response.json();
        return [null, result];
    } catch (error) {
        return [error.message, null];
    }
};
// Add new translation

// Remove all translations
export const clearTranslationsHistoryAsync = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [],
            }),
        });
        if (!response.ok) {
            throw new Error("Could not update orders.");
        }
        const result = await response.json();
        return [null, result];
    } catch (error) {
        return [error.message, null];
    }
};
