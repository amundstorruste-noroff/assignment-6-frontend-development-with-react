import { createHeaders } from "./index";

const apiUrl = process.env.REACT_APP_API_URL;

/**
 * Get a user given its username
 * @param {*} username 
 * @returns 
 */
const getUser = async (username) => {
  try {
    const response = await fetch(`${apiUrl}?username=${username}`);
    if (!response.ok) {
      throw new Error("Could not complete request.");
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

/**
 * Creates a new user with a given username and push it to the API. 
 * @param {*} username 
 * @returns 
 */
const createNewUser = async (username) => {
  try {
    const response = await fetch(apiUrl, {
        method: "POST",
        headers: createHeaders(),
        body: JSON.stringify({
            username,
            translations: []
        })
    });
    if (!response.ok) {
      throw new Error("Could not create user with username " + username);
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

/**
 * Login a user. Creates a user if it doesn't already exist.
 * @param {*} username
 * @returns
 */
export const loginUser = async (username) => {
  const [checkError, user] = await getUser(username);

  // If something goes wrong return error message
  if (checkError !== null) return [checkError, null]

  // Return user if exists
  if (user.length > 0) return [null, user.pop()]

  // Return newly created user if no user exists
  return await createNewUser(username)
};

/**
 * Returns the user object of a specific Id registered in the API.
 * @param {*} userId 
 * @returns 
 */
export const findUserById = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if (!response.ok) throw new Error("Could not fetch user.")
        const user = await response.json()
        return [null, user]
    } catch (error) {
        return [error.message, null]
    }
}
