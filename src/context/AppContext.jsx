// REsponsible for merging all the context in the app.
// In other words: Place the pyramid of doom here!

import UserProvider from "./UserContext";

const AppContext = (props) => {
  return (
    <>
      <UserProvider>{props.children}</UserProvider>
    </>
  );
};

export default AppContext;
