import { createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { readFromStorage } from "../utils/storage";

// Context object - exposing the state
const UserContext = createContext();

// Lets save ourselves some time and export the context object
/**
 * Returns the { user, setUser } state stored in the context api
 * @returns 
 */
export const useUser = () => {
  return useContext(UserContext); // { user, setUser}
};

// Provider object - manages the state
const UserProvider = (props) => {
  const [user, setUser] = useState(readFromStorage(STORAGE_KEY_USER));

  const state = {
    user,
    setUser,
  };

  return (
    <UserContext.Provider value={state}>{props.children}</UserContext.Provider>
  );
};

export default UserProvider;
