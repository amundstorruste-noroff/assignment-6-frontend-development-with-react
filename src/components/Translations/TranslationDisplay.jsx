import "./Translations.css";
import TranslationIcon from "./TranslationIcon";

export const TranslationDisplay = ({ translation }) => {
    const generateTranslationObjects = (sentance) => {
        const words = sentance.split(" ");

        const translationObjectList = words.map((word, wordIdx) => {
            const charArray = [...word];

            const translationObjects = charArray.map((letter, idx) => {
                return {
                    id: wordIdx + idx,
                    letter: letter,
                    imagePath: `individual_signs/${letter}.png`,
                };
            });
            return translationObjects;
        });

        // Generate TranslationIcons
        const translationDisplay = translationObjectList.map((translationObjects, idx) => {
            const translationIcons = translationObjects.map((translation) => {
                return <TranslationIcon key={translation.id} translation={translation}></TranslationIcon>;
            });

            return (
                <div className="translated-word" key={idx}>
                    {translationIcons}
                </div>
            );
        });
        return translationDisplay;
    };

    return (
        <div id="translation-display" className="flex-col">
            {translation && generateTranslationObjects(translation)}
        </div>
    );
};
