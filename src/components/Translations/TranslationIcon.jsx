const TranslationIcon = ({ translation }) => {
    return (
        <>
            <button className="translation-icon-button">
                <img src={translation.imagePath} alt={translation.letter}></img>
                <b>{translation.letter}</b>
            </button>
        </>
    );
};

export default TranslationIcon;
