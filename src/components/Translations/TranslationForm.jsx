import "./Translations.css";
import { useForm } from "react-hook-form";

export const TranslationForm = ({ onTranslation }) => {
    const { register, handleSubmit } = useForm();

    // Call on translation method from parent to execute translation
    const onSubmit = ({ sentenceToTranslate }) => onTranslation(sentenceToTranslate);

    return (
        <div id="translation-form" className="content">
            <h1>Translations</h1>
            <form id="translation-notes" onSubmit={handleSubmit(onSubmit)}>
                <fieldset className="flex-row gray-rounded-square">
                    <label className="flex-row" htmlFor="sentenceToTranslate">
                        <img id="keyboard" src="resources/keyboard-icon.png" alt="Keyboard"></img>
                        <div className="vertical-line"></div>
                    </label>
                    <input className="login-input" type="text" {...register("sentenceToTranslate")} placeholder="Translate a sentence..." />
                    <button className="login-button" type="submit">
                        &#8594;
                    </button>
                </fieldset>
            </form>
        </div>
    );
};
