const TranslationSummary = ({ translation }) => {
    return (
        <section>
            <h4>Translation summary</h4>
            <div>
                <img src={translation.imagePath} alt={translation.letter} width="55"></img>
            </div>
            <p>{translation.letter}</p>
        </section>
    );
};

export default TranslationSummary;
