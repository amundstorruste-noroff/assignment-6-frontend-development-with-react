import "./LoginForm.css";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { saveToStorage } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

// Optimization.
// If component rerenders, the object is not recreated. Only created once.
const usernameConfig = {
    required: true,
    minLength: 3,
};

const LoginForm = (props) => {
    // Hooks
    const {
        register,
        handleSubmit, // Prevents page from reloading
        formState: { errors },
    } = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    // Parent state
    const setLoading = (value) => props.setLoading(value);
    const setApiError = (value) => props.setApiError(value);

    // Redirect to profile if we're logged in.
    useEffect(() => {
        if (user !== null) {
            navigate("profile");
        }
    }, [user, navigate]);

    /**
     * Specify behaviour after loggin in. Either log in or create a new user.
     * @param {*} param0
     */
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username);

        // Save error if something is afoot!
        if (error !== null) setApiError(error);

        // Store user to local storage
        if (userResponse !== null) {
            saveToStorage(STORAGE_KEY_USER, userResponse);
            setUser(userResponse);
        }

        setLoading(false);
    };

    /**
     * Show error message is Username is missing, or too short (less than 3 characters)
     */
    const errorMessage = (() => {
        if (!errors.username) return null;
        if (errors.username.type === "required") {
            return <p id="login-error-message">Username is required</p>;
        }
        if (errors.username.type === "minLength") {
            return <p id="login-error-message">Username must be at least 3 characters</p>;
        }
    })();

    return (
        <>
        <div className="absolute-blobber flex-column">
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset className="flex-row gray-rounded-square">
                    <label className="flex-row" htmlFor="username">
                        <img id="keyboard" src="resources/keyboard-icon.png" alt="Keyboard"></img>
                        <div className="vertical-line"></div>
                    </label>
                    <input className="login-input" placeholder="What's your name?" type="text" {...register("username", usernameConfig)} />
                    <button className="login-button" type="submit" disabled={props.loading}>
                        &#8594;
                    </button>
                </fieldset>
            </form>
            <div className="purp-man"></div>
        </div>
        {errorMessage}
        </>
    );
};

export default LoginForm;
