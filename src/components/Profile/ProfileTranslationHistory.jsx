import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

const ProfileTranslationHistory = ({ translations }) => {
    const translationsList = translations.map((translation, index) => (
        <ProfileTranslationHistoryItem key={index + "-" + translation} translation={translation} />
    ));

    return (
        <div id="profile-translation-history">
            <h4>Your previous translations</h4>
            {translationsList.length === 0 && <p>You have no translations yet.</p>}
            <ul>{translationsList}</ul>
        </div>
    );
};

export default ProfileTranslationHistory;
