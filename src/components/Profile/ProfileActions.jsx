import { Link } from "react-router-dom";
import { clearTranslationsHistoryAsync } from "../../api/translate";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { deleteFromStorage, saveToStorage } from "../../utils/storage";

const ProfileActions = () => {
    const { user, setUser } = useUser();

    const handleClearHistoryClick = async () => {
        if (!window.confirm("Are you sure?\nThis can not be undone!")) {
            return;
        }

        const [clearError] = await clearTranslationsHistoryAsync(user.id);
        if (clearError !== null) {
            // Something bad happened, so stop
            return;
        }

        const updatedUser = {
            ...user,
            translations: [],
        };

        // Set local component state
        setUser(updatedUser);
        // Save to local context storage
        saveToStorage(STORAGE_KEY_USER, updatedUser);
    };

    const handleLogoutClick = () => {
        if (window.confirm("Are you sure?")) {
            // Send an event to the parent
            deleteFromStorage(STORAGE_KEY_USER);
            setUser(null);
        }
    };
    return (
        <ul>
            <li>
                <Link to="/translations">Translations</Link>
            </li>
            <li>
                <button onClick={handleClearHistoryClick}>Clear History</button>
            </li>
            <li>
                <button onClick={handleLogoutClick}>Logout</button>
            </li>
        </ul>
    );
};

export default ProfileActions;
