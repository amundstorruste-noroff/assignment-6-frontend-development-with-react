import "./Navbar.css";
import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";

const Navbar = () => {
    const { user } = useUser();

    return (
        <div className="navbar heading" className="yellow-background">
            <nav className="navbar heading content">
                {user !== null && (
                    <div className="navbarLogo">
                        <img className="over" src="resources/Logo.png" alt="Robot logo"></img>
                        <img className="under" src="resources/Splash.svg" alt="Background cloud"></img>
                    </div>
                )}
                <div>
                    <span>Lost in Translation</span>
                </div>
                {user !== null && (
                    <div className="profile-navbar">
                        <NavLink to="/profile">{user.username}</NavLink>
                        <img className="user-icon" src="resources/user.svg" alt="Profile user icon"></img>
                    </div>
                )}
                {user === null && <div></div>}
            </nav>
            <hr></hr>
        </div>
    );
};

export default Navbar;
