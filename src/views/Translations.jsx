import withAuth from "../hoc/withAuth";
import { TranslationForm } from "../components/Translations/TranslationForm";
import { TranslationDisplay } from "../components/Translations/TranslationDisplay";
import { useState } from "react";
import { useUser } from "../context/UserContext";
import { addTranslationAsync } from "../api/translate";
import { saveToStorage } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKeys";

const Translations = () => {
    // Local state
    const [translation, setTranslation] = useState(null);
    const { user, setUser } = useUser();

    const handleTranslateClick = async (sentence) => {
        // Check if we have a sentence
        if (!sentence) {
            alert("Please select something to translate first!");
            return;
        }

        // Set sentence to local state
        setTranslation(sentence);

        // Add new translation to current user. And store in API.
        const [error, updatedUser] = await addTranslationAsync(user, sentence.trim());

        // Check if something bad happened
        if (error !== null) {
            return;
        }

        // Set local component state
        setUser(updatedUser);
        // Save to local context storage
        saveToStorage(STORAGE_KEY_USER, updatedUser);
    };

    return (
        <div id="translations">
            <div className="yellow-background">
                <div className="content">
                    <TranslationForm onTranslation={handleTranslateClick}></TranslationForm>
                </div>
            </div>
            <div className="content">
                <TranslationDisplay translation={translation}></TranslationDisplay>
            </div>
        </div>
    );
};

// Wrap in withAuth() method to prowtect route
export default withAuth(Translations);
