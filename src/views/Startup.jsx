import { useState } from "react";
import LoginForm from "../components/Startup/LoginForm";

const Startup = () => {
    const [apiError, setApiError] = useState(null);
    const [loading, setLoading] = useState(false);

    return (
        <div id="startup">
            <div className="yellow-background">
                <div className="content">
                    <div className="welcomeBox">
                        <div>
                            <img id="welcome-logo" src="resources/Logo-Hello.png" alt="Robot Hello welcome message." />
                        </div>
                        <div>
                            <h1>Lost in Translation</h1>
                            <span> Get started</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="content">
                <LoginForm setApiError={(value) => setApiError(value)} setLoading={(value) => setLoading(value)} loading={loading} />
            </div>
            {loading && <p id="login-confirm-message">Logging in...</p>}
            {apiError && <p id="login-error-message">{apiError}</p>}
        </div>
    );
};

export default Startup;
