import "./Profile.css";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import withAuth from "../hoc/withAuth";
import { useUser } from "../context/UserContext";
import { useEffect } from "react";
import { findUserById } from "../api/user";
import { saveToStorage } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKeys";

const Profile = () => {
    const { user, setUser } = useUser();

    // Fetch translations every time the Profile component is rendered
    useEffect(() => {
        const findUser = async () => {
            const [error, latestUser] = await findUserById(user.id);
            if (error === null) {
                // Set local component state
                setUser(latestUser);
                // Save to local context storage
                saveToStorage(STORAGE_KEY_USER, latestUser);
            }
        };

        // findUser();
    }, [setUser, user.id]);

    return (
        <div id="profile">
            <div className="yellow-background">
                <div className="content">
                    <div id="profile-actions">
                        <h1>Profile: {user.username}</h1>
                        <ProfileActions />
                    </div>
                </div>
            </div>
            <div className="content">
                <ProfileTranslationHistory translations={user.translations} />
            </div>
        </div>
    );
};

// Wrap in withAuth() method to protect route
export default withAuth(Profile);
