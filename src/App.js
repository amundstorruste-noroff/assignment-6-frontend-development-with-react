import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Startup from "./views/Startup";
import Translations from "./views/Translations";
import Profile from "./views/Profile";
import Navbar from "./components/Navbar/Navbar";

function App() {
    return (
        <>
            <BrowserRouter>
                <Navbar />
                <div className="App">
                    <Routes>
                        <Route path="/" element={<Startup />} />
                        <Route path="/translations" element={<Translations />} />
                        <Route path="/profile" element={<Profile />} />
                    </Routes>
                </div>
            </BrowserRouter>

            {/* <div className="background"></div> */}
        </>
    );
}

export default App;
